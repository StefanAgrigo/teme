<?php

class Father extends Male
{
    public $n_kids;
    protected $job;

    public function __construct($name, $age, $CNP)
    {
        $this->name = $name;
        $this->age = $age;
        $CNP = $this->setCNP($CNP);
    }

    public function setKids($n_kids)
    {
        $this->n_kids = $n_kids;
    }

    public function getKids()
    {
        return $this->n_kids;
    }

    public function setJob($job)
    {
        $this->job = $job;
    }

    public function getJob()
    {
        return $this->job;
    }
//    public function showJob()
//    {
//        $job = $this->getJob();
//        echo $job;
//    }
}

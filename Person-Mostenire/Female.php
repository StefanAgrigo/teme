<?php

class Female extends Person
{
    public $sex;

    public function setSex($sex)
    {
        $this->sex = $sex;
    }

    public function getSex($sex)
    {
        return $this->sex;
    }
}
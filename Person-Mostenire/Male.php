<?php

class Male extends Person
{
    public $sex;

    public function setSex($sex)
    {
        $this->sex = $sex;
    }

    public function getSex()
    {
        return $this->sex;
    }
}

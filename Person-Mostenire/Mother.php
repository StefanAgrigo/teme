<?php

class Mother extends Female
{
    public $n_kids;
    protected $job;

    public function setKids($n_kids)
    {
        $this->n_kids = $n_kids;
    }

    public function getKids()
    {
        return $this->n_kids;
    }

    public function setJob($job)
    {
        $this->job = $job;
    }

    public function getJob()
    {
        return $this->job;
    }
    public function showJob()
    {
        $job = $this->getJob();
        echo $job;
    }
}
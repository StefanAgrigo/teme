<?php

class Person
{
    public $name;
    public $age;
    private $CNP;

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setAge($age)
    {
        $this->age = $age;
    }

    public function getAge()
    {
        return $this->age;
    }

    public function setCNP($CNP)
    {
        $this->CNP = $CNP;
    }

    public function getCNP()
    {
        return $this->CNP;
    }

    public function showCNP()
    {
        $CNP = $this->getCNP();
        echo "cnp:".$CNP;
    }
}